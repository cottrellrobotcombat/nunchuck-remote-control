#include "SH1106SPi.h"

#include <esp_now.h>
#include <WiFi.h>

#include "bargraph.h"
#include "mac_addresses.h"

#include <NintendoExtensionCtrl.h>

Nunchuk nchuk;

BarGraph* leftMotorBar  = NULL;
BarGraph* leftArmBar    = NULL;
BarGraph* rightMotorBar = NULL;
BarGraph* rightArmBar   = NULL;

constexpr size_t MAC_LENGTH = 6;

const uint8_t* MY_MAC = REMOTE_MAC;
const uint8_t* PEER_MAC = ROBOT_MAC;
esp_now_peer_info_t peerInfo;

const int DISPLAY_SPI_RES = 17;
const int DISPLAY_SPI_DC = 16;
const int DISPLAY_SPI_CS = 5;

SH1106Spi display(DISPLAY_SPI_RES, DISPLAY_SPI_DC, DISPLAY_SPI_CS);

struct ControlMessage {
  int8_t leftMotor = 0;
  int8_t rightMotor = 0;
  int8_t leftArm = 0;
  int8_t rightArm = 0;
  bool enable = false;
  uint8_t sync = 0;
};

struct ReportMessage {
  uint16_t batteryMillivolts = 0;
  bool isEnabled = false;
  uint8_t sync = 0;
};

ControlMessage outgoingMessage;
ReportMessage incomingMessage;

constexpr size_t INCOMING_LENGTH = sizeof(incomingMessage);
constexpr size_t OUTGOING_LENGTH = sizeof(outgoingMessage);

void onDataTx(const uint8_t* mac, esp_now_send_status_t status) {
  Serial.print("Last message callback status: ");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "SUCCESS" : "FAIL");
}

unsigned long millisAtLastRx = 0;

void onDataRx(const uint8_t* mac, const uint8_t* incomingData, int len) {
  if (len == INCOMING_LENGTH) {
    memcpy(&incomingMessage, incomingData, INCOMING_LENGTH);
    millisAtLastRx = millis();
  }
  else {
    Serial.print("Unexpected incoming datasize: ");
    Serial.print(len);
    Serial.print(", expected ");
    Serial.print(INCOMING_LENGTH);
  }
}

bool checkMacAddressIs(const uint8_t* ref) {
  // Memory allocation (bytes and string)
  uint8_t mac[MAC_LENGTH];
  char macStr[MAC_LENGTH*3] = { 0 };
  // Get system MAC addr, print it and check against ref
  WiFi.macAddress(mac);
  sprintf(macStr, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
  Serial.print("My MAC is: ");
  Serial.println(String(macStr));
  bool mismatch = false;
  for (size_t i = 0; i < MAC_LENGTH; ++i)
    mismatch |= mac[i] != ref[i];
  // If didn't match, print the expectation too
  if (mismatch) {
    sprintf(macStr, "%02X:%02X:%02X:%02X:%02X:%02X", ref[0], ref[1], ref[2], ref[3], ref[4], ref[5]);
    Serial.print("Expected:  ");
    Serial.println(String(macStr));
  }
  return !mismatch;
}

void setup() {
  Serial.begin(115200);
  Serial.print("\n\n");

  Serial.print("Incoming length: ");
  Serial.println(INCOMING_LENGTH);
  Serial.print("Outgoing length: ");
  Serial.println(OUTGOING_LENGTH);

  // Initialising the UI will init the display too.
  display.init();

  // display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.flipScreenVertically();

  WiFi.mode(WIFI_STA);
  checkMacAddressIs(MY_MAC);
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initialising ESP NOW");
    return;
  }
  else {
    Serial.println("Successfully init ESP NOW");
  }

  esp_now_register_send_cb(onDataTx);
  esp_now_register_recv_cb(onDataRx);

  memcpy(peerInfo.peer_addr, PEER_MAC, 6);
  peerInfo.channel = 0;
  peerInfo.encrypt = false;

  if (esp_now_add_peer(&peerInfo) != ESP_OK) {
    Serial.println("Failed to add peer");
  }
  else {
    Serial.println("Added peer successfully");
  }

  nchuk.begin();

	while (!nchuk.connect()) {
		Serial.println("Nunchuk not detected!");
		delay(1000);
	}

  leftMotorBar  = new BarGraph(  1, 33,  8, 30, 127, -127);
  leftArmBar    = new BarGraph(  1,  0,  8, 30, 127, -127);
  rightMotorBar = new BarGraph(118, 33,  8, 30, 127, -127);
  rightArmBar   = new BarGraph(118,  0,  8, 30, 127, -127);
}

float clamp(float in, float lower, float upper) {
  return (in < lower) ? lower : ((in > upper) ? upper : in);
}

// Y stick goes 32 to 224
// X stick goes 29 to 234
// so both are roughly 128 +/- 95

int8_t stickToDrive(int nintendoStick) {
  float distFromCentre = nintendoStick - 128.0F;
  distFromCentre *= 137.0F / 95.0;  // We want +/- 95 to become +/- 137 (will be clipped)
  distFromCentre = roundf(distFromCentre);
  return clamp(int(distFromCentre), -127, 127); // clip at limits
}

constexpr float ANTI_FRICTION_THRESH = 50.0F;  // About double min movement power

void mixer(int upVal, int sideVal, int& spdTgtL, int& spdTgtR) {

  // If stick outside of centre but below a mid-power threshold...
  if ((abs(upVal) > 4 || abs(sideVal) > 12) && abs(upVal) < ANTI_FRICTION_THRESH) {
    // ... halve distance between upVal and thresh
    int boostAmt = (ANTI_FRICTION_THRESH - float(abs(upVal))) / 2;
    upVal += upVal < 0 ? -boostAmt : boostAmt; // maintain symmetry for backwards
  }

  spdTgtL = upVal * ((-127 - sideVal) / -127.0F);
  spdTgtR = upVal * ((127 - sideVal) / 127.0F);

  if (upVal > 0 && spdTgtL > upVal)
    spdTgtL = upVal;
  if (upVal > 0 && spdTgtR > upVal)
    spdTgtR = upVal;
  if (upVal < 0 && spdTgtL < upVal)
    spdTgtL = upVal;
  if (upVal < 0 && spdTgtR < upVal)
    spdTgtR = upVal;
}


const int DELAY_PER_LOOP_US = 20000;
const int MINIMUM_RX_DROPOUT_MS = 500;

// For text going onto screen
static char tmpBuff[32];

constexpr int FLIPPER_DOWN_NONINV = -60;
constexpr int FLIPPER_UP_NONINV = 127;

constexpr int FLIPPER_DOWN_INV = 50;
constexpr int FLIPPER_UP_INV = -90;

bool lastButtonC = false;

bool inverted = false;

void loop() {

  nchuk.update();
  
  bool c = nchuk.buttonC();
  if (c && !lastButtonC) {
    inverted = !inverted;
  }
  lastButtonC = c;

  int spdTgtL, spdTgtR;

  int inv_mult = inverted ? -1 : 1;

  mixer(inv_mult * stickToDrive(nchuk.joyY()),
        inv_mult * stickToDrive(nchuk.joyX()),
        spdTgtL,
        spdTgtR);

  bool z = nchuk.buttonZ();

  int flipperUp = inverted ? FLIPPER_UP_INV : FLIPPER_UP_NONINV;
  int flipperDown = inverted ? FLIPPER_DOWN_INV : FLIPPER_DOWN_NONINV;

  // Pack Message  
  outgoingMessage.leftMotor = spdTgtL;
  outgoingMessage.rightMotor = spdTgtR;
  outgoingMessage.leftArm = z ? flipperUp : flipperDown;
  outgoingMessage.rightArm =  z ? flipperUp : flipperDown;
  outgoingMessage.enable = true;
  outgoingMessage.sync++;

  // Send Message
  esp_err_t result = esp_now_send(PEER_MAC, (uint8_t *) &outgoingMessage, sizeof(outgoingMessage));
  if (result != ESP_OK) {
    Serial.println("Sending failed");
  }

  // Update bar graph states
  leftMotorBar->update(outgoingMessage.leftMotor);
  leftArmBar->update(outgoingMessage.leftArm);
  rightMotorBar->update(outgoingMessage.rightMotor);
  rightArmBar->update(outgoingMessage.rightArm);

  // GUI
  display.clear();

  display.drawString(41,0,"BitsBotRC");

  if (inverted) {
    display.drawString(55, 12, "INV");
  }

  if (millis() > millisAtLastRx + MINIMUM_RX_DROPOUT_MS) {
    display.drawString(35,30, "NO COMMS");
  }
  else {
    display.drawStringf(35,20,tmpBuff, "%d mV", incomingMessage.batteryMillivolts);
    display.drawString(35,30, incomingMessage.isEnabled ? "Enabled" : "Disabled");
  }

  // Show comm sync values
  display.drawStringf(45,50, tmpBuff, "%03d/%03d", outgoingMessage.sync, incomingMessage.sync);

  // Control numbers
  display.drawStringf(16, 42, tmpBuff, "%+04d", outgoingMessage.leftMotor);
  display.drawStringf(16,  8, tmpBuff, "%+04d", outgoingMessage.leftArm);
  display.drawStringf(88, 42, tmpBuff, "%+04d", outgoingMessage.rightMotor);
  display.drawStringf(88,  8, tmpBuff, "%+04d", outgoingMessage.rightArm);
  
  // Control bar graphs
  display.fillRect(leftMotorBar->x(), leftMotorBar->y(), leftMotorBar->w(), leftMotorBar->h());
  display.fillRect(leftArmBar->x(), leftArmBar->y(), leftArmBar->w(), leftArmBar->h());
  display.fillRect(rightMotorBar->x(), rightMotorBar->y(), rightMotorBar->w(), rightMotorBar->h());
  display.fillRect(rightArmBar->x(), rightArmBar->y(), rightArmBar->w(), rightArmBar->h());

  // Tick lines
  display.drawLine(10, leftMotorBar->tickHeight(), 13, leftMotorBar->tickHeight());
  display.drawLine(10, leftArmBar->tickHeight(), 13, leftArmBar->tickHeight());
  display.drawLine(113, rightMotorBar->tickHeight(), 116, rightMotorBar->tickHeight());
  display.drawLine(113, rightArmBar->tickHeight(), 116, rightArmBar->tickHeight());

  display.display();

  // Pause before next loop
  delayMicroseconds(DELAY_PER_LOOP_US);

}
